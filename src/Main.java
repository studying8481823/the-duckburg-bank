import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        greetings();
        giveMeAmount();
        giveMeTerm();
        monthRate();
        double monthlyPayment = calculateMonthlyPayment();
        System.out.printf("Your monthly payment is: %.2f\n", monthlyPayment);
    }

    static double yearRate = 1.0;
    static int amount = 0;
    static int term = 0;
    static double r = 0;

    // greetings method
    // can be adjusted or changed
    public static void greetings() {
        System.out.println("Hello, dear client!");
        System.out.println("Now you are at the page that helps you to calculate your loan.");
    }

    // method that asks from client the amount of loan money
    public static void giveMeAmount () {
        Scanner reader = new Scanner(System.in);
        while (true) {
            System.out.print("Please, give us a desirable amount of money: ");
            amount = Integer.parseInt(reader.nextLine());
            if (amount > 0) {
                break;
            } else {
                System.out.println("Principal amount must be positive. Please try again.");
            }
        }
    }

    // method that asks from client the term of loan
    public static void giveMeTerm () {
        Scanner reader = new Scanner(System.in);
        while (true) {
            System.out.print("Now specify the term of the loan. Give us the term in month (1-300): ");
            term = Integer.parseInt(reader.nextLine());
            if (term > 0 && term <= 300) {
                break;
            } else {
                System.out.println("Loan term must be between 1 and 300 months. Please try again.");
            }
        }
        reader.close();
    }


    //calculating the month rate of credit
    public static double monthRate() {
        r = yearRate/12/100;
        return r;
    }

    //calculating the monthly payment
    public static double calculateMonthlyPayment () {
        return ((amount*r)* Math.pow((1+r), term)) / (Math.pow((1+r), term) - 1);
    }
}